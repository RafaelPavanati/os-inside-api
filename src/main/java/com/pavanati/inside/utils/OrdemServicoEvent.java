package com.pavanati.inside.utils;

import com.pavanati.inside.model.OrdemServico;
import com.pavanati.inside.services.SendEmailClienteService;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.Objects;

import static java.util.Objects.isNull;

@Stateless
public class OrdemServicoEvent {

    @Inject
    private SendEmailClienteService sendEmailClienteService;

    @Asynchronous
    public void alteraOrdemServico(@Observes OrdemServico ordemServico) {
        if (!isNull(ordemServico.getCliente().getEmail())) {
            sendEmailClienteService.sendEmail(ordemServico);
        }
    }
}
