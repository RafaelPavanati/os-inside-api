package com.pavanati.inside.utils;


import javax.annotation.Priority;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.lang.reflect.Method;

@Auditavel
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
public class Auditor {


    @AroundInvoke
    public Object auditar(InvocationContext context) throws Exception {


        Method method = context.getMethod();
        Object target = context.getTarget();
        Object[] parameters = context.getParameters();

       return context.proceed();
    }
}
