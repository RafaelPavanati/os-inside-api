package com.pavanati.inside.enums;

import com.pavanati.inside.utils.EnumType;

public enum TipoServico implements EnumType {

    ORCAMENTO("Orçamento"),
    GARANTIA("Garantia"),
    RETORNO("Retorno");

    private final String key;



    TipoServico(String key) {
        this.key = key;
    }


    @Override
    public String getKey() {
        return key;
    }

//    @Converter(autoApply = true)
//    public static final class TipoServicoConverter extends EnumConverter<TipoServico> {
//        public TipoServicoConverter() {
//            super(TipoServico.class);
//        }
//    }
}
