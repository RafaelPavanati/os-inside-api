package com.pavanati.inside.enums;

import com.pavanati.inside.utils.EnumConverter;
import com.pavanati.inside.utils.EnumType;

import javax.persistence.Converter;

public enum Situacao implements EnumType {

    AGUARDANDO_ANALISE("aguardando analise"),
    AGUARDANDO_APROVACAO("Aguardando aprovação"),
    AGUARDANDO_PECA("Aguardando peça"),
    AGUARDANDO_CONSERTO("Aguardando conserto"),
    PRONTO("pronto"),
    ENTREGUE("entregue");

    private final String key;



    Situacao(String key) {
        this.key = key;
    }

    @Override
    public String getKey() {
        return key;
    }



    @Converter(autoApply = true)
    public static final class SituacaoConverter extends EnumConverter<Situacao> {
        public SituacaoConverter() {
            super(Situacao.class);
        }
    }

}
