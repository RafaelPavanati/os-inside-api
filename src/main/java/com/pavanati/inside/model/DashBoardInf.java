package com.pavanati.inside.model;


import java.util.List;

public class DashBoardInf {

    private List<OrdemServico> osParada;


    private List<Integer> retornoMeses;

    private List<String> meses;

    private Integer prontos;



    private Integer orcamentos;

    private Integer garantias;

    private Integer retornos;


    public Integer getRetornos() {
        return retornos;
    }



    public Integer getProntos() {
        return prontos;
    }

    public Integer getOrcamentos() {
        return orcamentos;
    }

    public Integer getGarantias() {
        return garantias;
    }


    public List<OrdemServico> getOsParada() {
        return osParada;
    }

    public DashBoardInf setOsParada(List<OrdemServico> osParada) {
        this.osParada = osParada;
        return this;
    }

    public List<String> getMeses() {
        return meses;
    }

    public DashBoardInf setMeses(List<String> meses) {
        this.meses = meses;
        return this;
    }

    public List<Integer> getRetornoMeses() {
        return retornoMeses;
    }

    public DashBoardInf setRetornoMeses(List<Integer> retornoMeses) {
        this.retornoMeses = retornoMeses;
        return this;
    }

    public DashBoardInf setRetornos(Integer retornos) {
        this.retornos = retornos;
        return this;
    }

    public DashBoardInf setProntos(Integer prontos) {
        this.prontos = prontos;
        return this;
    }

    public DashBoardInf setOrcamentos(Integer orcamentos) {
        this.orcamentos = orcamentos;
        return this;
    }

    public DashBoardInf setGarantias(Integer garantias) {
        this.garantias = garantias;
        return this;
    }


    public DashBoardInf setRetornos(int i) {
        this.retornos = i;
        return  this;
    }
}
