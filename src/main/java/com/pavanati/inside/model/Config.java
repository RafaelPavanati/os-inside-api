package com.pavanati.inside.model;


import javax.persistence.*;

@Entity
@Table(name = "CONFIG")
@SequenceGenerator(name = "CONFIG_SEQ", sequenceName = "CONFIG_SEQ", allocationSize = 1)
public class Config implements Entidade {

    @Id
    @Column(name = "ID_CONFIG")
    @GeneratedValue(generator = "CONFIG_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;


    @Column(name = "DIAS_INATIVO")
    private  int diasOsInativa;

    @Column(name = "EMAIL")
    private String emailSend;

    @Column(name = "SENHA_EMAIL")
    private String senhaEmailSend;

    @Column(name = "NOME_EMPRESA")
    private String nomeEmpresa;

    @Column(name = "TELEFONE")
    private String telefone;

    @Override
    public Long getId() {
        return id;
    }

    public Config setId(Long id) {
        this.id = id;
        return this;
    }


    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public Config setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
        return this;
    }

    public int getDiasOsInativa() {
        return diasOsInativa;
    }

    public Config setDiasOsInativa(int diasOsInativa) {
        this.diasOsInativa = diasOsInativa;
        return this;
    }

    public String getEmailSend() {
        return emailSend;
    }

    public Config setEmailSend(String emailSend) {
        this.emailSend = emailSend;
        return this;
    }

    public String getSenhaEmailSend() {
        return senhaEmailSend;
    }

    public Config setSenhaEmailSend(String senhaEmailSend) {
        this.senhaEmailSend = senhaEmailSend;
        return this;
    }
}
