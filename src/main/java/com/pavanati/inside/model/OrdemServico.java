package com.pavanati.inside.model;

import com.pavanati.inside.enums.Situacao;
import com.pavanati.inside.enums.TipoServico;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "ORDEM_SERVICO")
@SequenceGenerator(name = "ORDER_SERVICO_SEQ", sequenceName = "ORDER_SERVICO_SEQ", allocationSize = 1)
public class OrdemServico implements Entidade {
    
    @Id
    @Column(name = "ID_ORDER_SERVICO")
    @GeneratedValue(generator = "ORDER_SERVICO_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;


    @NotNull
    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private Cliente cliente;

    @NotNull
    @Column(name = "NOME")
    private String nome;

    @Column(name = "DATA_ABERTURA")
    private LocalDateTime abertura;

    @Column(name = "DATA_MOVIMENTO")
    private LocalDateTime dataAlteracao = LocalDateTime.now();

    @Column(name = "MARCA")
    private String marca;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_SERVICO")
    private TipoServico tipoServico;

    @Enumerated(EnumType.STRING)
    @Column(name = "SITUACAO")
    private Situacao situacao;

    @Column(name = "MODELO")
    private String modelo;

    @Column(name = "SERIE")
    private String serie;

    @Column(name = "DEFEITO")
    private String defeito;

    @Column(name = "SERVICO")
    private String servico;

    @Column(name = "VALOR")
    private BigDecimal valor;

    @Column(name = "CONSERVACAO")
    private String conservacao;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "CONTROLE")
    private String controle;

    @Column(name = "PRECO")
    private BigDecimal preco;

    @Column(name = "PAGAMENTO_REALIZADO")
    private Boolean pagamentoRealizado;



    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getPagamentoRealizado() {
        return pagamentoRealizado;
    }

    public OrdemServico setPagamentoRealizado(Boolean pagamentoRealizado) {
        this.pagamentoRealizado = pagamentoRealizado;
        return this;
    }

    public LocalDateTime getDataAlteracao() {
        return dataAlteracao;
    }

    public String getDefeito() {
        return defeito;
    }

    public OrdemServico setDefeito(String defeito) {
        this.defeito = defeito;
        return this;
    }

    public String getServico() {
        return servico;
    }

    public OrdemServico setServico(String servico) {
        this.servico = servico;
        return this;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public OrdemServico setValor(BigDecimal valor) {
        this.valor = valor;
        return this;
    }

    public String getConservacao() {
        return conservacao;
    }

    public OrdemServico setConservacao(String conservacao) {
        this.conservacao = conservacao;
        return this;
    }

    public String getControle() {
        return controle;
    }

    public OrdemServico setControle(String controle) {
        this.controle = controle;
        return this;
    }

    public OrdemServico setDataAlteracao(LocalDateTime dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
        return this;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public String getNome() {
        return nome;
    }

    public String getMarca() {
        return marca;
    }

    public TipoServico getTipoServico() {
        return tipoServico;
    }

    public LocalDateTime getAbertura() {
        return abertura;
    }

    public OrdemServico setAbertura(LocalDateTime abertura) {
        this.abertura = abertura;
        return this;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public OrdemServico setSituacao(Situacao situacao) {
        this.situacao = situacao;
        return this;
    }

    public OrdemServico setTipoServico(TipoServico tipoServico) {
        this.tipoServico = tipoServico;
        return this;
    }

    public String getModelo() {
        return modelo;
    }

    public String getSerie() {
        return serie;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }


    public OrdemServico setCliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    public OrdemServico setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public OrdemServico setMarca(String marca) {
        this.marca = marca;
        return this;
    }



    public OrdemServico setModelo(String modelo) {
        this.modelo = modelo;
        return this;
    }

    public OrdemServico setSerie(String serie) {
        this.serie = serie;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);

        hash = 23 * hash + Objects.hashCode(this.descricao);
        hash = 23 * hash + Objects.hashCode(this.preco);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Produto{" + "id=" + id + ", codigo=" +  ", descricao=" + descricao + ", preco=" + preco + ", quantidade="  + '}';
    }
    
}
