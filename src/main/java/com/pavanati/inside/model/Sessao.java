package com.pavanati.inside.model;

public class Sessao {

    private final String nomeUsuario;
    private final String token;

    public Sessao(String nomeUsuario, String token) {
        this.nomeUsuario = nomeUsuario;
        this.token = token;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public String getToken() {
        return token;
    }


}
