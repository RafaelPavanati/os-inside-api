package com.pavanati.inside.model;

import java.io.Serializable;

public interface Entidade extends Serializable {
    Long getId();
}
