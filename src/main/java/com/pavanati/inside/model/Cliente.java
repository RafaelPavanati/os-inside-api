package com.pavanati.inside.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "CLIENTES")
@SequenceGenerator(name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ", allocationSize = 1)
public class Cliente implements Entidade {
    
    @Id
    @Column(name = "ID_CLIENTE")
    @GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @Column(name = "DOCUMENTO")
    private String documento;

    @Column(name = "RG")
    private String rg;
    
    @Column(name = "NOME")
    private String nome;

    @Column(name = "TELEFONE" )
    private String telefone;

    @Column(name = "DATA_CADASTRO" )
    private LocalDateTime datacadastro = LocalDateTime.now();

    @Column(name = "TELEFONE_2" )
    private String telefone2;

    @Embedded
    private Endereco enderecoPessoal;

    @OneToMany
    @JoinColumn(name = "CLIENTE_ID")
    private List<OrdemServico> servicos;

    @Column(name = "EMAIL", length = 100)
    private String email;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNome() {
        return nome;
    }

    public Cliente setNome(@NotNull String nome) {
        this.nome = nome;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public Cliente setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public LocalDateTime getDatacadastro() {
        return datacadastro;
    }

    public Cliente setDatacadastro(LocalDateTime datacadastro) {
        this.datacadastro = datacadastro;
        return this;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public Cliente setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Cliente setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getRg() {
        return rg;
    }

    public Cliente setRg(String rg) {
        this.rg = rg;
        return this;
    }




    public Endereco getEnderecoPessoal() {
        return enderecoPessoal;
    }

    public Cliente setEnderecoPessoal(Endereco enderecoPessoal) {
        this.enderecoPessoal = enderecoPessoal;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.documento);
        hash = 83 * hash + Objects.hashCode(this.nome);
        hash = 83 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.documento, other.documento)) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }

        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", documento=" + documento + ", nome=" + nome + ", email=" + email + '}';
    }
}
