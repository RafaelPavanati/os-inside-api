package com.pavanati.inside.services;

import com.pavanati.inside.model.Cliente;
import com.pavanati.inside.utils.GenericDao;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class ClienteService extends AbstractCrudService<Cliente> {
    
    @Inject
    private GenericDao<Cliente> dao;

    @Inject
    private EntityManager em;


    @Override
    protected GenericDao<Cliente> getDao() {
        return dao;
    }

    public EntityManager getEm() {
        return em;
    }


}
