package com.pavanati.inside.services;

import com.pavanati.inside.model.OrdemServico;
import com.pavanati.inside.utils.GenericDao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.validation.Valid;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class OrdemServicoService extends AbstractCrudService<OrdemServico> {

    @Inject
    private EntityManager em;



    @Inject
    private Event<OrdemServico> eventoOrdemServicoAlterada;

    @Inject
    public GenericDao<OrdemServico> dao;

    @Override
    protected GenericDao<OrdemServico> getDao() {
        return dao;
    }

    public EntityManager getEm() {
        return em;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public OrdemServico update(@Valid OrdemServico bean) {
        eventoOrdemServicoAlterada.fire(bean);
        return getDao().update(bean);
    }
}
