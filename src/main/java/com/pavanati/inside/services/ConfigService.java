package com.pavanati.inside.services;

import com.pavanati.inside.model.Config;
import com.pavanati.inside.utils.GenericDao;
import com.pavanati.inside.utils.JpaCriteriaHelper;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.Optional;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class ConfigService extends AbstractCrudService<Config> {

    @Inject
    private GenericDao<Config> dao;

    @Inject
    private EntityManager em;

    @Override
    protected GenericDao<Config> getDao() {
        return dao;
    }


    public Optional findConfig(){
        JpaCriteriaHelper helper = JpaCriteriaHelper.select(em, Config.class);
      return   helper.getFirstResultOpt();

  //    return  em.find(Config.class,"id_config");
    }
}
