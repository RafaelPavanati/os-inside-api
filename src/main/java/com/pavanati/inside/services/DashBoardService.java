package com.pavanati.inside.services;

import com.pavanati.inside.model.Cliente;
import com.pavanati.inside.model.Config;
import com.pavanati.inside.model.DashBoardInf;
import com.pavanati.inside.model.OrdemServico;
import com.pavanati.inside.utils.GenericDao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class DashBoardService  {


    @Inject
    private OrdemServicoService service;

    @Inject
    private ConfigService configService;

    @Inject
    private GenericDao<Cliente> dao;


    @Inject
    private EntityManager em;

    @Transactional
    public DashBoardInf find() {

        Optional<Config> config = configService.findConfig();

        List<OrdemServico> semMovimentação = service.getEm()
                .createNativeQuery("select * from ordem_servico where situacao != 'ENTREGUE'" +
                        "and DATA_MOVIMENTO <= ?", OrdemServico.class)
                .setParameter(1, LocalDate.now().plusDays(- (config.isPresent() ? config.get().getDiasOsInativa() : 30 ))).getResultList();


        Integer orcamentos = Integer.valueOf(service.getEm().createNativeQuery("select COUNT(ID_ORDER_SERVICO)  from ordem_servico where TIPO_SERVICO = 'ORCAMENTO'").getSingleResult().toString());
        Integer garantias = Integer.valueOf(service.getEm().createNativeQuery("select COUNT(ID_ORDER_SERVICO)  from ordem_servico where TIPO_SERVICO = 'GARANTIA'").getSingleResult().toString());
        Integer retornos = Integer.valueOf(service.getEm().createNativeQuery("select COUNT(ID_ORDER_SERVICO)  from ordem_servico where TIPO_SERVICO = 'RETORNO'").getSingleResult().toString());



        List<Integer> retornosMeses = new ArrayList<>();
        List<String> meses = new ArrayList<>();

//        for (int i = 0; i < 6; i++) {
//            Integer retornoMesX = Integer.valueOf(service.getEm().createNativeQuery("select COUNT(ID_ORDER_SERVICO)  from ordem_servico where TIPO_SERVICO = 'RETORNO' and DATA_ABERTURA <= ? and DATA_ABERTURA >= ?")
//                    .setParameter(1, LocalDate.now().plusMonths(-(i)))
//                    .setParameter(2, LocalDate.now().plusMonths(-(i+1)))
//                    .getSingleResult().toString());
//            meses.add(LocalDate.now().plusMonths(-(i)).getMonth().toString());
//            retornosMeses.add(retornoMesX);
//        }


        return new DashBoardInf()
                .setOsParada(semMovimentação)
                .setMeses(meses)
                .setRetornoMeses(retornosMeses)
                .setGarantias(garantias)
                .setOrcamentos(orcamentos)
                .setRetornos(retornos);
    }
}
