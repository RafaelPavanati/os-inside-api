package com.pavanati.inside.services;


import com.pavanati.inside.model.Config;
import com.pavanati.inside.model.OrdemServico;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Optional;
import java.util.Properties;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class SendEmailClienteService {

    @Inject
    private ConfigService configService;


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void sendEmail(OrdemServico osCliente) {
        //para usar o email deve-se permitir acesso no Gmail;
        //https://myaccount.google.com/u/2/lesssecureapps?pli=1&pageId=none

        Optional<Config> config = configService.findConfig();

        if (config.isPresent()) {
            final String username = config.get().getEmailSend();
            final String password = config.get().getSenhaEmailSend();

            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Authenticator authenticator = new Authenticator() {
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            };

            try {
                Session session = Session.getDefaultInstance(props, authenticator);
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(config.get().getEmailSend()));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(osCliente.getCliente().getEmail()));
                //message.
                message.setSubject("Situacão equipamento na "+ config.get().getNomeEmpresa() + " com ordem de servico numero " + osCliente.getId());
                message.setText("Estatus atual da ordem de serviço,"
                        + "\n\n Tipo de Serviço : " + osCliente.getTipoServico().getKey()
                        + "\n\n Nome Cliente : " + osCliente.getCliente().getNome()
                        + "\n\n Equipamento : " + osCliente.getNome()
                        + "\n\n Marca : " + osCliente.getMarca()
                        + "\n\n Série :" + osCliente.getSerie()
                        + "\n\n Defeito : " + osCliente.getDefeito()
                        + "\n\n Situação : " + osCliente.getSituacao().getKey()
                );

                Transport.send(message);

                System.out.println("Done");

            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }

        }
    }
}
