package com.pavanati.inside.rest.resources;

import com.pavanati.inside.model.Cliente;
import com.pavanati.inside.rest.AbstractCrudResource;
import com.pavanati.inside.services.AbstractCrudService;
import com.pavanati.inside.services.ClienteService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static java.util.Objects.isNull;

@Path("clientes")
public class ClienteResource extends AbstractCrudResource<Cliente> {

    @Inject
    private ClienteService service;

    @Override
    protected AbstractCrudService<Cliente> getService() {
        return service;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(
            @QueryParam("page")
            @DefaultValue("1") Integer pageNumber,
            @QueryParam("size")
            @DefaultValue("15") Integer pageSize,
            @QueryParam("filterField") String filterField,
            @QueryParam("filterValue") String filterValue,
            @QueryParam("order") String order) {


        if (!isNull(filterField) && filterField.equals("cliente")) {
            List resultList = service.getEm().createQuery("SELECT c FROM Cliente c WHERE upper(c.nome) like upper(?) OR  upper(c.email) like upper(?) OR" +
                    " c.documento like ?  OR  c.rg like ?   OR  c.telefone like ?  ",Cliente.class)
                    .setParameter(1 , "%" + filterValue + "%" )
                    .setParameter(2 , "%" + filterValue + "%" )
                    .setParameter(3 , "%" + filterValue + "%" )
                    .setParameter(4 , "%" + filterValue + "%"  )
                    .setParameter(5 , "%" + filterValue + "%" ).getResultList();
            Response.Status responseStatus = (pageNumber * pageSize < resultList.size()) ? Response.Status.PARTIAL_CONTENT : Response.Status.OK;
            Response response = Response.status(responseStatus)
                    .entity(resultList).build();

            response.getHeaders().add("X-Total-Lenght", resultList.size());
            response.getHeaders().add("X-Page-Size", pageSize);
            response.getHeaders().add("X-Current-Page", pageNumber);
            return response;
        } else {
            Long total = getService().getCount(filterField, filterValue);
            Response.Status responseStatus = (pageNumber * pageSize < total) ? Response.Status.PARTIAL_CONTENT : Response.Status.OK;
            Response response = Response.status(responseStatus)
                    .entity(getService().findAll(pageSize, pageNumber, filterField, filterValue, order)).build();
            response.getHeaders().add("X-Total-Lenght", total);
            response.getHeaders().add("X-Page-Size", pageSize);
            response.getHeaders().add("X-Current-Page", pageNumber);
            return response;
        }
    }

}
