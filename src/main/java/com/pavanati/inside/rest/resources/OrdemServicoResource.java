package com.pavanati.inside.rest.resources;

import com.pavanati.inside.enums.Situacao;
import com.pavanati.inside.model.OrdemServico;
import com.pavanati.inside.rest.AbstractCrudResource;
import com.pavanati.inside.services.AbstractCrudService;
import com.pavanati.inside.services.OrdemServicoService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.Objects.isNull;

@Path("ordem-servico")
public class OrdemServicoResource extends AbstractCrudResource<OrdemServico> {

    @Inject
    private OrdemServicoService service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(
            @QueryParam("page")
            @DefaultValue("1") Integer pageNumber,
            @QueryParam("size")
            @DefaultValue("15") Integer pageSize,
            @QueryParam("filterField") String filterField,
            @QueryParam("filterValue") String filterValue,
            @QueryParam("order") String order) {


        if (!isNull(filterField) && filterField.equals("cliente")) {

            List resultList = service.getEm().createNativeQuery("select * " +
                    "from ordem_servico  JOIN clientes ON " +
                    "(clientes.id_cliente = ordem_servico.id_cliente ) " +
                    "WHERE clientes.nome Ilike ? OR clientes.telefone Ilike ? " +
                    " OR clientes.documento Ilike ? ORDER BY DATA_ABERTURA desc", OrdemServico.class)
                    .setParameter(1, filterValue)
                    .setParameter(2, filterValue)
                    .setParameter(3, filterValue).getResultList();
            Response.Status responseStatus = (pageNumber * pageSize < resultList.size()) ? Response.Status.PARTIAL_CONTENT : Response.Status.OK;
            Response response = Response.status(responseStatus)
                    .entity(resultList).build();

            response.getHeaders().add("X-Total-Lenght", resultList.size());
            response.getHeaders().add("X-Page-Size", pageSize);
            response.getHeaders().add("X-Current-Page", pageNumber);
            return response;
        } else {
            Long total = getService().getCount(filterField, filterValue);
            Response.Status responseStatus = (pageNumber * pageSize < total) ? Response.Status.PARTIAL_CONTENT : Response.Status.OK;
            Response response = Response.status(responseStatus)
                    .entity(getService().findAll(pageSize, pageNumber, filterField, filterValue, order)).build();
            response.getHeaders().add("X-Total-Lenght", total);
            response.getHeaders().add("X-Page-Size", pageSize);
            response.getHeaders().add("X-Current-Page", pageNumber);
            return response;
        }
    }

    @Path("imprimir")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Response imprimir(OrdemServico bean) throws JRException, ClassNotFoundException, SQLException {


        String source = "C:/inside-os-ordem-de-servico2";

        String driver = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost:5432/inside-os";
        String login = "postgres";
        String senha = "admin";

            Class.forName(driver);
            Connection con = DriverManager.getConnection(url, login, senha);
        JasperPrint jasperPrint = JasperFillManager.fillReport(source, null, con);

        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
        jasperViewer.setVisible(true);


        return null;
    }
        @POST
        @Produces(MediaType.APPLICATION_JSON)
        @Consumes(MediaType.APPLICATION_JSON)
        @TransactionAttribute(TransactionAttributeType.REQUIRED)
        public Response insert (OrdemServico bean){

            OrdemServico ordemServico = new OrdemServico()
                    .setTipoServico(bean.getTipoServico())
                    .setAbertura(LocalDateTime.now())
                    .setCliente(bean.getCliente())
                    .setDataAlteracao(LocalDateTime.now())
                    .setSituacao(Situacao.AGUARDANDO_ANALISE)
                    .setMarca(bean.getMarca())
                    .setModelo(bean.getModelo())
                    .setNome(bean.getNome())
                    .setSerie(bean.getSerie());

            return Response.status(Response.Status.CREATED)
                    .entity(getService().insert(ordemServico))
                    .build();

        }

        @Override
        protected AbstractCrudService<OrdemServico> getService () {
            return service;
        }

    }
