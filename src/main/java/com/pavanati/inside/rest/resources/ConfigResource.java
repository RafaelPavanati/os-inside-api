package com.pavanati.inside.rest.resources;

import com.pavanati.inside.model.Config;
import com.pavanati.inside.rest.AbstractCrudResource;
import com.pavanati.inside.services.AbstractCrudService;
import com.pavanati.inside.services.ConfigService;

import javax.inject.Inject;
import javax.ws.rs.Path;

@Path("config")
public class ConfigResource extends AbstractCrudResource<Config>  {

    @Inject
    private ConfigService service;

    @Override
    protected AbstractCrudService<Config> getService() {
        return service;
    }
}
