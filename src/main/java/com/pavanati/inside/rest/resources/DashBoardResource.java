package com.pavanati.inside.rest.resources;


import com.pavanati.inside.model.DashBoardInf;
import com.pavanati.inside.services.DashBoardService;
import com.pavanati.inside.services.OrdemServicoService;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("dashboard")
@Transactional
public class DashBoardResource {

    @Inject
    private OrdemServicoService service;

    @Inject
    DashBoardService dashBoardService;



    @Path("/inf")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public DashBoardInf index() {
     return   dashBoardService.find();
    }


}
